package com.synrgy.ujian

fun main() {
    polaHurufJ(5)
    polaKetupat(5)
    polaJamPasir(5)
}

fun polaHurufJ(numberOfRowsOrColumns: Int) {
    // Membuat judul menggunakan Raw String
    println(
        """
    =================
    
         Huruf J 
    -----------------
    """.trimIndent()
    )

    /**
     * Looping for topShaperOfLetterJ -> Membuat bagian ATAS huruf J (Segitiga terbalik 180 derajat)
     *
     * Alur:
     * topShaperOfLetterJ = 5 -> Mencetak spasi sebanyak 0 kali (tidak dicetak pada looping pertama) dan • (titik) sebanyak 9 kali secara horizontal
     * topShaperOfLetterJ = 4 -> Mencetak spasi sebanyak 2 kali dan • (titik) sebanyak 7 kali secara horizontal
     * topShaperOfLetterJ = 3 -> Mencetak spasi sebanyak 4 kali dan • (titik) sebanyak 5 kali secara horizontal
     * topShaperOfLetterJ = 2 -> Mencetak spasi sebanyak 6 kali dan • (titik) sebanyak 3 kali secara horizontal
     * topShaperOfLetterJ = 1 -> Mencetak spasi sebanyak 8 kali dan • (titik) sebanyak 1 kali secara horizontal
     */
    for (topShaperOfLetterJ in numberOfRowsOrColumns downTo 1) {
        /**
         * Looping for helperSpace -> Mencetak spasi hingga membentuk Segitiga Siku-Siku
         *
         * Alur:
         * helperSpace = 5, topShaperOfLetterJ(5) + 1 = 6 (tidak memenuhi kondisi) -> Mencetak spasi sebanyak 0 kali (tidak dicetak pada looping pertama)
         * helperSpace = 5, topShaperOfLetterJ(4) + 1 = 5 -> Mencetak spasi sebanyak 2 kali
         * helperSpace = 5, topShaperOfLetterJ(3) + 1 = 4 -> Mencetak spasi sebanyak 4 kali
         * helperSpace = 5, topShaperOfLetterJ(2) + 1 = 3 -> Mencetak spasi sebanyak 6 kali
         * helperSpace = 5, topShaperOfLetterJ(1) + 1 = 2 -> Mencetak spasi sebanyak 8 kali
         */
        for (helperSpace in numberOfRowsOrColumns downTo topShaperOfLetterJ + 1) {
            // Mencetak 2 kali spasi
            print("  ")
        }

        /**
         * Looping for dotPrinter -> Mencetak • (titik) hingga membentuk Segitiga terbalik 180 derajat
         *
         * Alur:
         * dotPrinter = 1, topShaperOfLetterJ(5) * 2 = 10 menjadi 9 (karena until) -> Mencetak • (titik) sebanyak 9 kali secara horizontal
         * dotPrinter = 2, topShaperOfLetterJ(4) * 2 = 8 menjadi 7 (karena until)  -> Mencetak • (titik) sebanyak 7 kali secara horizontal
         * dotPrinter = 3, topShaperOfLetterJ(3) * 2 = 6 menjadi 5 (karena until)  -> Mencetak • (titik) sebanyak 5 kali secara horizontal
         * dotPrinter = 4, topShaperOfLetterJ(2) * 2 = 4 menjadi 3 (karena until)  -> Mencetak • (titik) sebanyak 3 kali secara horizontal
         * dotPrinter = 5, topShaperOfLetterJ(1) * 2 = 2 menjadi 1 (karena until)  -> Mencetak • (titik) sebanyak 1 kali secara horizontal
         */
        for (dotPrinter in 1 until topShaperOfLetterJ * 2) {
            // Mencetak • (titik) dan 1 kali spasi
            print("• ")
        }

        // Membuat variabel topShaperOfLetterJ tercetak kebawah (vertikal)
        println()
    }

    // ==================================================================================================== //

    /**
     * Looping for bottomShaperOfLetterJ -> Membuat bagian BAWAH huruf J (Jajar Genjang)
     *
     * Alur:
     * bottomShaperOfLetterJ = 1 -> Mencetak spasi sebanyak 10 kali dan • (titik) sebanyak 4 kali secara horizontal
     * bottomShaperOfLetterJ = 2  -> Mencetak spasi sebanyak 8 kali dan • (titik) sebanyak 4 kali secara horizontal
     * bottomShaperOfLetterJ = 3  -> Mencetak spasi sebanyak 6 kali dan • (titik) sebanyak 4 kali secara horizontal
     * bottomShaperOfLetterJ = 4  -> Mencetak spasi sebanyak 4 kali dan • (titik) sebanyak 4 kali secara horizontal
     * bottomShaperOfLetterJ = 5  -> Mencetak spasi sebanyak 2 kali dan • (titik) sebanyak 4 kali secara horizontal
     */
    for (bottomShaperOfLetterJ in 1..numberOfRowsOrColumns) {
        /**
         * Looping for helperSpace -> Mencetak spasi hingga membentuk Segitiga Siku-Siku terbalik
         *
         * Alur:
         * helperSpace = 5 ke bottomShaperOfLetterJ(5) -> Mencetak spasi sebanyak 10 kali (karena print langsung 2 kali spasi dalam 1 looping)
         * helperSpace = 5 ke bottomShaperOfLetterJ(4)  -> Mencetak spasi sebanyak 8 kali (karena print langsung 2 kali spasi dalam 1 looping)
         * helperSpace = 5 ke bottomShaperOfLetterJ(3)  -> Mencetak spasi sebanyak 6 kali (karena print langsung 2 kali spasi dalam 1 looping)
         * helperSpace = 5 ke bottomShaperOfLetterJ(2)  -> Mencetak spasi sebanyak 4 kali (karena print langsung 2 kali spasi dalam 1 looping)
         * helperSpace = 5 ke bottomShaperOfLetterJ(1)  -> Mencetak spasi sebanyak 2 kali (karena print langsung 2 kali spasi dalam 1 looping)
         */
        for (helperSpace in numberOfRowsOrColumns downTo bottomShaperOfLetterJ) {
            // Mencetak 2 kali spasi
            print("  ")
        }

        /**
         * Looping for dotPrinter -> Mencetak • (titik) hingga membentuk Jajar Genjang
         *
         * Alur:
         * dotPrinter = 1 sampai argumen(5) menjadi 4 (karena until) -> Mencetak • (titik) sebanyak 4 kali secara horizontal
         * dotPrinter = 2 sampai argumen(5) menjadi 4 (karena until) -> Mencetak • (titik) sebanyak 4 kali secara horizontal
         * dotPrinter = 3 sampai argumen(5) menjadi 4 (karena until) -> Mencetak • (titik) sebanyak 4 kali secara horizontal
         * dotPrinter = 4 sampai argumen(5) menjadi 4 (karena until) -> Mencetak • (titik) sebanyak 4 kali secara horizontal
         * dotPrinter = 5 sampai argumen(5) menjadi 4 (karena until) -> Mencetak • (titik) sebanyak 4 kali secara horizontal
         */
        for (dotPrinter in 1 until numberOfRowsOrColumns) {
            // Mencetak • (titik) dan 1 kali spasi
            print("• ")
        }

        // Membuat variabel bottomShaperOfLetterJ tercetak kebawah (vertikal)
        println()
    }

    println("\n=================\n")
}

fun polaKetupat(numberOfRowsOrColumns: Int) {
    // Membuat judul menggunakan Raw String
    println(
        """
         Ketupat
    -----------------
    """.trimIndent()
    )

    /**
     * Looping for topShaperOfRiceCake -> Membuat bagian ATAS Ketupat (Segitiga Sama Sisi)
     *
     * Alur:
     * topShaperOfRiceCake = 1 -> Mencetak spasi sebanyak 8 kali dan • (titik) sebanyak 1 kali secara horizontal
     * topShaperOfRiceCake = 2 -> Mencetak spasi sebanyak 6 kali dan • (titik) sebanyak 3 kali secara horizontal
     * topShaperOfRiceCake = 3 -> Mencetak spasi sebanyak 4 kali dan • (titik) sebanyak 5 kali secara horizontal
     * topShaperOfRiceCake = 4 -> Mencetak spasi sebanyak 2 kali dan • (titik) sebanyak 7 kali secara horizontal
     * topShaperOfRiceCake = 5 -> Mencetak spasi sebanyak 0 kali (tidak dicetak pada looping terakhir) dan • (titik) sebanyak 9 kali secara horizontal
     */
    for (topShaperOfRiceCake in 1..numberOfRowsOrColumns) {
        /**
         * Looping for helperSpace -> Mencetak spasi hingga membentuk Segitiga Siku-Siku terbalik
         *
         * Alur:
         * helperSpace = 1, numberOfRowsOrColumns(5) - 1 -> Mencetak spasi sebanyak 8 kali
         * helperSpace = 2, numberOfRowsOrColumns(5) - 2 -> Mencetak spasi sebanyak 6 kali
         * helperSpace = 3, numberOfRowsOrColumns(5) - 3 -> Mencetak spasi sebanyak 4 kali
         * helperSpace = 4, numberOfRowsOrColumns(5) - 4 -> Mencetak spasi sebanyak 2 kali
         * helperSpace = 5, numberOfRowsOrColumns(5) - 5 -> Mencetak spasi sebanyak 0 kali (tidak dicetak pada looping terakhir)
         */
        for (helperSpace in 1..numberOfRowsOrColumns - topShaperOfRiceCake) {
            // Mencetak 2 kali spasi
            print("  ")
        }

        /**
         * Looping for dotPrinter -> Mencetak • (titik) hingga membentuk Segitiga Sama Sisi
         *
         * Alur:
         * dotPrinter = 1, topShaperOfRiceCake(1) * 2 = 2 menjadi 1 (karena until)  -> Mencetak • (titik) sebanyak 1 kali secara horizontal
         * dotPrinter = 2, topShaperOfRiceCake(2) * 2 = 4 menjadi 3 (karena until)  -> Mencetak • (titik) sebanyak 3 kali secara horizontal
         * dotPrinter = 3, topShaperOfRiceCake(3) * 2 = 6 menjadi 5 (karena until)  -> Mencetak • (titik) sebanyak 5 kali secara horizontal
         * dotPrinter = 4, topShaperOfRiceCake(4) * 2 = 8 menjadi 7 (karena until)  -> Mencetak • (titik) sebanyak 7 kali secara horizontal
         * dotPrinter = 5, topShaperOfRiceCake(5) * 2 = 10 menjadi 9 (karena until) -> Mencetak • (titik) sebanyak 9 kali secara horizontal
         */
        for (dotPrinter in 1 until topShaperOfRiceCake * 2) {
            // Mencetak • (titik) dan 1 kali spasi
            print("• ")
        }

        // Membuat variabel topShaperOfRiceCake tercetak kebawah (vertikal)
        println()
    }

    // ==================================================================================================== //

    /**
     * Looping for bottomShaperOfRiceCake -> Membuat bagian ATAS huruf J (Segitiga terbalik 180 derajat)
     *
     * Alur:
     * bottomShaperOfRiceCake = 5 -> Mencetak spasi sebanyak 0 kali (tidak dicetak pada looping pertama) dan • (titik) sebanyak 9 kali secara horizontal
     * bottomShaperOfRiceCake = 4 -> Mencetak spasi sebanyak 2 kali dan • (titik) sebanyak 7 kali secara horizontal
     * bottomShaperOfRiceCake = 3 -> Mencetak spasi sebanyak 4 kali dan • (titik) sebanyak 5 kali secara horizontal
     * bottomShaperOfRiceCake = 2 -> Mencetak spasi sebanyak 6 kali dan • (titik) sebanyak 3 kali secara horizontal
     * bottomShaperOfRiceCake = 1 -> Mencetak spasi sebanyak 8 kali dan • (titik) sebanyak 1 kali secara horizontal
     */
    for (bottomShaperOfRiceCake in numberOfRowsOrColumns downTo 1) {
        /**
         * Looping for helperSpace -> Mencetak spasi hingga membentuk Segitiga Siku-Siku
         *
         * Alur:
         * helperSpace = 5, bottomShaperOfRiceCake(5) + 1 = 6 -> Spasi tercetak sebanyak 0 kali (tidak dicetak pada looping pertama)
         * helperSpace = 5, bottomShaperOfRiceCake(4) + 1 = 5 -> Spasi tercetak sebanyak 2 kali
         * helperSpace = 5, bottomShaperOfRiceCake(3) + 1 = 4 -> Spasi tercetak sebanyak 4 kali
         * helperSpace = 5, bottomShaperOfRiceCake(2) + 1 = 3 -> Spasi tercetak sebanyak 6 kali
         * helperSpace = 5, bottomShaperOfRiceCake(1) + 1 = 2 -> Spasi tercetak sebanyak 8 kali
         */
        for (helperSpace in numberOfRowsOrColumns downTo bottomShaperOfRiceCake + 1) {
            // Mencetak 2 kali spasi
            print("  ")
        }

        /**
         * Looping for dotPrinter -> Mencetak • (titik) hingga membentuk Segitiga terbalik 180 derajat
         *
         * Alur:
         * dotPrinter = 1, bottomShaperOfRiceCake(5) * 2 = 10 menjadi 9 (karena until) -> Mencetak • (titik) sebanyak 9 kali secara horizontal
         * dotPrinter = 2, bottomShaperOfRiceCake(4) * 2 = 8 menjadi 7 (karena until)  -> Mencetak • (titik) sebanyak 7 kali secara horizontal
         * dotPrinter = 3, bottomShaperOfRiceCake(3) * 2 = 6 menjadi 5 (karena until)  -> Mencetak • (titik) sebanyak 5 kali secara horizontal
         * dotPrinter = 4, bottomShaperOfRiceCake(2) * 2 = 4 menjadi 3 (karena until)  -> Mencetak • (titik) sebanyak 3 kali secara horizontal
         * dotPrinter = 5, bottomShaperOfRiceCake(1) * 2 = 2 menjadi 1 (karena until)  -> Mencetak • (titik) sebanyak 1 kali secara horizontal
         */
        for (dotPrinter in 1 until bottomShaperOfRiceCake * 2) {
            // Mencetak • (titik) dan 1 kali spasi
            print("• ")
        }

        // Membuat variabel bottomShaperOfRiceCake tercetak kebawah (vertikal)
        println()
    }

    println("\n=================\n")
}

fun polaJamPasir(numberOfRowsOrColumns: Int) {
    // Membuat judul menggunakan Raw String
    println(
        """
        Jam Pasir
    -----------------
    """.trimIndent()
    )

    /**
     * Looping for topShaperOfHourglass -> Membuat bagian ATAS Jam Pasir (Segitiga terbalik 180 derajat)
     *
     * Alur:
     * topShaperOfHourglass = 5 -> Mencetak spasi sebanyak 0 kali (tidak dicetak pada looping pertama) dan • (titik) sebanyak 9 kali secara horizontal
     * topShaperOfHourglass = 4 -> Mencetak spasi sebanyak 2 kali dan • (titik) sebanyak 7 kali secara horizontal
     * topShaperOfHourglass = 3 -> Mencetak spasi sebanyak 4 kali dan • (titik) sebanyak 5 kali secara horizontal
     * topShaperOfHourglass = 2 -> Mencetak spasi sebanyak 6 kali dan • (titik) sebanyak 3 kali secara horizontal
     * topShaperOfHourglass = 1 -> Mencetak spasi sebanyak 8 kali dan • (titik) sebanyak 1 kali secara horizontal
     */
    for (topShaperOfHourglass in numberOfRowsOrColumns downTo 1) {
        /**
         * Looping for helperSpace -> Mencetak spasi hingga membentuk Segitiga Siku-Siku
         *
         * Alur:
         * helperSpace = 5, topShaperOfHourglass(5) + 1 = 6 (tidak memenuhi kondisi) -> Spasi tercetak sebanyak 0 kali (tidak dicetak pada looping pertama)
         * helperSpace = 5, topShaperOfHourglass(4) + 1 = 5 -> Spasi tercetak sebanyak 2 kali
         * helperSpace = 5, topShaperOfHourglass(3) + 1 = 4 -> Spasi tercetak sebanyak 4 kali
         * helperSpace = 5, topShaperOfHourglass(2) + 1 = 3 -> Spasi tercetak sebanyak 6 kali
         * helperSpace = 5, topShaperOfHourglass(1) + 1 = 2 -> Spasi tercetak sebanyak 8 kali
         */
        for (helperSpace in numberOfRowsOrColumns downTo topShaperOfHourglass + 1) {
            // Mencetak 2 kali spasi
            print("  ")
        }

        /**
         * Looping for dotPrinter -> Mencetak • (titik) hingga membentuk Segitiga terbalik 180 derajat
         *
         * Alur:
         * dotPrinter = 1, topShaperOfHourglass(5) * 2 = 10 menjadi 9 (karena until) -> Mencetak • (titik) sebanyak 9 kali secara horizontal
         * dotPrinter = 2, topShaperOfHourglass(4) * 2 = 8 menjadi 7 (karena until)  -> Mencetak • (titik) sebanyak 7 kali secara horizontal
         * dotPrinter = 3, topShaperOfHourglass(3) * 2 = 6 menjadi 5 (karena until)  -> Mencetak • (titik) sebanyak 5 kali secara horizontal
         * dotPrinter = 4, topShaperOfHourglass(2) * 2 = 4 menjadi 3 (karena until)  -> Mencetak • (titik) sebanyak 3 kali secara horizontal
         * dotPrinter = 5, topShaperOfHourglass(1) * 2 = 2 menjadi 1 (karena until)  -> Mencetak • (titik) sebanyak 1 kali secara horizontal
         */
        for (dotPrinter in 1 until topShaperOfHourglass * 2) {
            // Mencetak • (titik) dan 1 kali spasi
            print("• ")
        }

        // Membuat variabel topShaperOfHourglass tercetak kebawah (vertikal)
        println()
    }

    // ==================================================================================================== //

    /**
     * Looping for bottomShaperOfHourglass -> Membuat bagian ATAS Ketupat (Segitiga Sama Sisi)
     *
     * Alur:
     * bottomShaperOfHourglass = 1 -> Mencetak spasi sebanyak 8 kali dan • (titik) sebanyak 1 kali secara horizontal
     * bottomShaperOfHourglass = 2 -> Mencetak spasi sebanyak 6 kali dan • (titik) sebanyak 3 kali secara horizontal
     * bottomShaperOfHourglass = 3 -> Mencetak spasi sebanyak 4 kali dan • (titik) sebanyak 5 kali secara horizontal
     * bottomShaperOfHourglass = 4 -> Mencetak spasi sebanyak 2 kali dan • (titik) sebanyak 7 kali secara horizontal
     * bottomShaperOfHourglass = 5 -> Mencetak spasi sebanyak 0 kali (tidak dicetak pada looping terakhir) dan • (titik) sebanyak 9 kali secara horizontal
     */
    for (bottomShaperOfHourglass in 1..numberOfRowsOrColumns) {
        /**
         * Looping for helperSpace -> Mencetak spasi hingga membentuk Segitiga Siku-Siku terbalik
         *
         * Alur:
         * helperSpace = 1, numberOfRowsOrColumns(5) - 1 -> Mencetak spasi sebanyak 8 kali
         * helperSpace = 2, numberOfRowsOrColumns(5) - 2 -> Mencetak spasi sebanyak 6 kali
         * helperSpace = 3, numberOfRowsOrColumns(5) - 3 -> Mencetak spasi sebanyak 4 kali
         * helperSpace = 4, numberOfRowsOrColumns(5) - 4 -> Mencetak spasi sebanyak 2 kali
         * helperSpace = 5, numberOfRowsOrColumns(5) - 5 -> Mencetak spasi sebanyak 0 kali (tidak dicetak pada looping terakhir)
         */
        for (helperSpace in 1..numberOfRowsOrColumns - bottomShaperOfHourglass) {
            // Mencetak 2 kali spasi
            print("  ")
        }

        /**
         * Looping for dotPrinter -> Mencetak • (titik) hingga membentuk Segitiga Sama Sisi
         *
         * Alur:
         * dotPrinter = 1, bottomShaperOfHourglass(1) * 2 = 2 menjadi 1 (karena until)  -> Mencetak • (titik) sebanyak 1 kali secara horizontal
         * dotPrinter = 2, bottomShaperOfHourglass(2) * 2 = 4 menjadi 3 (karena until)  -> Mencetak • (titik) sebanyak 3 kali secara horizontal
         * dotPrinter = 3, bottomShaperOfHourglass(3) * 2 = 6 menjadi 5 (karena until)  -> Mencetak • (titik) sebanyak 5 kali secara horizontal
         * dotPrinter = 4, bottomShaperOfHourglass(4) * 2 = 8 menjadi 7 (karena until)  -> Mencetak • (titik) sebanyak 7 kali secara horizontal
         * dotPrinter = 5, bottomShaperOfHourglass(5) * 2 = 10 menjadi 9 (karena until) -> Mencetak • (titik) sebanyak 9 kali secara horizontal
         */
        for (dotPrinter in 1 until bottomShaperOfHourglass * 2) {
            // Mencetak • (titik) dan 1 kali spasi
            print("• ")
        }

        // Membuat variabel bottomShaperOfHourglass tercetak kebawah (vertikal)
        println()
    }

    print("\n=================")
}