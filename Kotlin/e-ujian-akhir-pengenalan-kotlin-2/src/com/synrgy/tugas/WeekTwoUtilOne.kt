package com.synrgy.tugas

import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

object WeekTwoUtilOne {
  //TODO: buat function ekstention untuk membantu main function
  /*
  A. mengganti inputan harga dll dalam format rupiah
  B. merapihkan nama inputan barang (capital atau title case) *untuk poin ini tolong sepakati ke 3 group tidak boleh sama
  */

  fun Number.setToRupiah(): String {
    // Negara
    val indonesia = "ID"
    // Bahasa
    val bahasa = "id"

    return NumberFormat.getCurrencyInstance(Locale(bahasa, indonesia)).format(this)
  }

  fun String.capitalizeCase(): String {
    return this.trim().replaceFirstChar { it.uppercase() }
  }

  fun getDate(): String {
    val pattern = "dd-MM-yyyy                       HH:mm"
    val simpleDateFormat = SimpleDateFormat(pattern)
    return simpleDateFormat.format(Date())
  }
}