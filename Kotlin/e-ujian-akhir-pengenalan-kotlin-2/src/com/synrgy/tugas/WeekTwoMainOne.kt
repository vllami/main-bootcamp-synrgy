package com.synrgy.tugas

import com.synrgy.tugas.WeekTwoUtilOne.capitalizeCase
import com.synrgy.tugas.WeekTwoUtilOne.setToRupiah

/**Team one
Anggota
Villa, Siti, Mufti, Audi
Tema = Toko Makanan**/

const val YES = "y"

class WeekTwoMainOne {
    /* 1. ada inputan penjual berupa, Nama Barang, Harga, Jumlah stok barang (mas villa)
     * 2. ada inputan pembeli berupa, Nama dan jumlah yang dibeli (mbak siti)*/
    // TODO : Buat fungsi inputanan penjual dan pembeli dengan tema makanan

    private val dataProducts = HashMap<String, Barang>()
    private val dataBuyer = HashMap<String, Barang>()

    // Menampilkan pesan selamat datang pada penjual
    fun showMessageForSeller() {
        println(
            """
                    Selamat Datang Penjual     
            ---- Warung Makan || Nikmat Abadi ----
            """.trimIndent()
        )
    }

    // Menginput data produk bagi penjual
    fun inputDataSeller() {
        do {
            println(
                """
          
                       * Masukkan Detail Item *
                --------------------------------------
                """.trimIndent()
            )
            print("Nama Item: ")
            val itemName = readLine().toString()
            print("Harga 1 Porsi: Rp")
            val itemPrice = readLine()?.toDouble()
            print("Jumlah Stok Tersisa: ")
            val itemStock = readLine()?.toInt()

            val item = Barang(
                hargaBarang = itemPrice,
                jumlahBarang = itemStock
            )

            dataProducts[itemName] = item

            println("--------------------------------------")
            // Jika ingin menambahkan data produk silahkan tekan y dan karakter sembarang jika tidak
            print("Ingin Tambah Lagi? (y/t): ")
            val isAddItem = readLine()
        } while (isAddItem == YES)
    }

    // Menampilkan daftar produk jualan yang telah diinputkan penjual
    fun showProductList() {
        println(
            """
      
            ---------- Daftar Transaksi ----------
            ${WeekTwoUtilOne.getDate()}
            ======================================
            """.trimIndent()
        )
        dataProducts.forEach { (key, value) ->
            println(
                """
                Nama Item     = ${key.capitalizeCase()}
                Stok Tersisa  = ${value.jumlahBarang}
                Harga 1 Porsi = ${value.hargaBarang!!.setToRupiah()}
                --------------------------------------
                """.trimIndent()
            )
        }
    }

    // Menampilkan pesan selamat datang untuk pembeli
    fun showMessageForBuyer() {
        println(
            """
      
      
                    Selamat Datang Pembeli     
            ---- Warung Makan || Nikmat Abadi ----
            """.trimIndent()
        )
    }

    // Menampilkan pesan input dan men-input data pembelian
    fun inputDataBuyer() {
        do {
            println(
                """
          
                     * Masukkan Detail Item *
                --------------------------------------
                """.trimIndent()
            )
            print("Nama Item: ")
            val itemName = readLine().toString()
            print("Jumlah Item: ")
            val itemQty = readLine()?.toInt()

            if (isProductAvailable(itemName) && isItemsLessThanStock(itemName, itemQty)) {
                dataBuyer[itemName] = Barang(dataProducts[itemName]?.hargaBarang, itemQty)
            }

            // Tekan y jika ingin menambahkan data belian dan karakter sembarang jika tidak
            print("Ada Item Lain? (y/t): ")
            val isAddItem = readLine()
        } while (isAddItem == YES)
    }

    // Mengecek apakah item tersedia, berlaku untuk pembeli
    private fun isProductAvailable(namaBarang: String): Boolean =
        if (dataProducts.containsKey(namaBarang)) {
            println("Item tersedia!"); true
        } else {
            println("Maaf, item ini tidak tersedia!"); false
        }

    // Mengecek apakah target pembelian sesuai dengan jumlah stok
    private fun isItemsLessThanStock(namaBarang: String, target: Int?): Boolean {
        val jumlahBarang = dataProducts[namaBarang]?.jumlahBarang!!

        return if (jumlahBarang >= target!!) {
            println("Stok tersedia!"); true
        } else {
            println("Maaf, item ini hanya tersisa $jumlahBarang item"); false
        }
    }

    fun paidOrNot() {
        print("Apakah ingin membayar? (y/t): ")
        val paid = readLine().toString()

        if (paid == YES) {
            printReceipt()
        } else {
            println("\n\n======================================")
            println("=========== DATANG KEMBALI ===========")
            println("======================================\n\n")
        }
    }

    private fun printReceipt() {
        var totalBarang = 0
        var totalHarga = 0.0

        println(
            """
      
            ------------ Nikmat Abadi ------------
            ${WeekTwoUtilOne.getDate()}
            ============ TANDA TERIMA ============
            """.trimIndent()
        )

        dataBuyer.forEach { (key, value) ->
            println("${key.capitalizeCase()} | ${value.jumlahBarang}x | ${(value.hargaBarang!! * value.jumlahBarang!!).setToRupiah()}")

            totalBarang += value.jumlahBarang
            totalHarga += value.hargaBarang * value.jumlahBarang
        }

        println(
            """
            --------------------------------------
            TOTAL       | ${totalBarang}x |       ${totalHarga.setToRupiah()}
            """.trimIndent()
        )
    }

    data class Barang(val hargaBarang: Double?, val jumlahBarang: Int?)
}

fun main() {
    WeekTwoMainOne().apply {
        // Penjual
        showMessageForSeller()
        inputDataSeller()
        showProductList()

        // Pembeli
        showMessageForBuyer()
        inputDataBuyer()
        paidOrNot()
    }
}

//TODO: buat function sesuai kebutuhan untuk mengecek kondisi penjualan
/*fun kondisi dan looping
* 1. handle jika stok barang kurang dari permintaan pembelian (mas audi)
* 2. handle total pembelian (mbak siti)
* 3. handle barang yang dibeli tidak ada (mas mufti)
* 4. buat extention function pada util per group untuk : (mas audi)
*   A. mengganti inputan harga dll dalam format rupiah
*   B. merapihkan nama inputan barang (capital atau title case)
* 5. print dengan foramt seperti pada struk belanja (mas villa)*/
//NOTE : kondisi dan looping saat mengecek dibuat beraneka ragam jangan sama dengan kelompok lain