package com.synrgy.tugas

fun main() {
    fibonacciSequence(100)
}

fun fibonacciSequence(panjang: Int) {
    var firstNum = 1
    var secondNum = firstNum

    if (panjang > 100) {
        print("Batas 100 deret")
    } else {
        print(firstNum)
        for (i in firstNum..panjang) {
            val formula = firstNum + secondNum

            print(" $secondNum")
            firstNum = secondNum
            secondNum = formula
        }
    }
}