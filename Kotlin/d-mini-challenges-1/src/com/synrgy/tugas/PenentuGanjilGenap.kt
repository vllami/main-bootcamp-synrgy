package com.synrgy.tugas

fun main() {
    oddEvenDetermination(6)
}

fun oddEvenDetermination(angka: Int) {
    if (angka % 2 == 0) {
        println("Angka $angka adalah Bilangan GENAP")
    } else {
        println("Angka $angka adalah Bilangan GANJIL")
    }
}