package com.synrgy.tugas

fun main() {
    var a = 5

    while (a >= 1) {
        var b = 5
        var c = 1

        while (b > a) {
            print("  ")
            b--
        }

        while (c < a * 2) {
            print("* ")
            c++
        }

        println()
        a--
    }
}