package com.synrgy.tugas

fun main() {
    for (a in 5 downTo 1) {
        for (b in 5 downTo a + 1) {
            print("  ")
        }

        for (c in 1 until a * 2) {
            print("* ")
        }

        println()
    }
}