package com.synrgy.tugas

fun main() {
    var a = 5

    do {
        var b = 5
        var c = 1

        while (b > a) {
            print("  ")
            b--
        }

        do {
            print("* ")
            c++
        } while (c < a * 2)

        println()
        a--
    } while (a >= 1)
}