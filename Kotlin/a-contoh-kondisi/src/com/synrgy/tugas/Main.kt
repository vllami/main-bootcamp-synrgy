package com.synrgy.tugas

fun main() {
    // IF-ELSE Expression
    mauMacBookPro("Belum Kerja")
    hadiahUntukAdik(60)
    perkembanganAdik(9)
    predikatTranskrip(90)
    adikBikinKTP(17)
    cekJenjangPendidikan(16)

    // WHEN Expression
    luwakWhiteCoffee("Kopi Nikmat Nyaman di Minum")
    sukaJajan(50_000)
    syaratWahanaPermainan(21)
    cekKelulusan(90)
}

// =========================================== IF-ELSE Expression ===========================================

/**
 * Pada fungsi ini, saya bercerita bahwa saya ingin membeli MacBook Pro untuk menunjang impian saya menjadi
 * Mobile Development yang menguasai Android-Kotlin, iOS, dan Flutter. Namun, untuk mewujudkan hal tersebut
 * saya harus bekerja keras untuk bisa membelinya.
 */
fun mauMacBookPro(tapiStatusmu: String) {
    // IF-ELSE IF Expression: Biasa
    if (tapiStatusmu == "Belum Kerja") {
        println("Kamu masih $tapiStatusmu, jadi belum bisa beli MacBook Pro")
    } else if (tapiStatusmu == "Kerja") {
        println("Karena sudah $tapiStatusmu, jadi udah bisa beli MacBook Pro")
    } else {
        println("Belum bisa beli atau emang belum mau beli?")
    }
}

/**
 * Pada fungsi ini, saya bercerita bahwa saya memiliki seorang Adik yang belum sekolah SD. Saya bilang, jika
 * ia sudah SD saya akan berikan hadiah jika ia mampu mencapai nilai yang telah saya tentukan.
 */
fun hadiahUntukAdik(jikaNilainya: Int) {
    // IF-ELSE IF Expression: Operator Range
    if (jikaNilainya in 70..80) {
        println("Belikan es krim kesukaanya")
    } else if (jikaNilainya in 80..90) {
        println("Belikan bantal kesukaanya")
    } else if (jikaNilainya in 90..100) {
        println("Belikan boneka kesukaanya")
    } else {
        println("Berikan nasihat agar rajin belajar")
    }
}

/**
 * Pada fungsi ini, saya bercerita bahwa jika Adik saya sudah SD dan telah mempelajari Matematika. Saya akan
 * bertanya kepadanya apakah ia bisa membedakan mana angka genap dan mana angka ganjil.
 */
fun perkembanganAdik(tentukanAngka: Int) {
    // IF-ELSE IF Expression: Operator Range dengan Step/Langkah
    if (tentukanAngka in 0..10 step 2) {
        println("\"Adikku: Angka $tentukanAngka itu angka Genap mas\"")
    } else if (tentukanAngka in 0..10 step 1) {
        println("\"Adikku: Angka $tentukanAngka itu angka Ganjil mas\"")
    } else {
        println("Materi ini masih terlalu sulit untuk Adik")
    }
}

/**
 * Pada fungsi ini, saya bercerita jika di suatu semester saya mampu mendapatkan nilai sekian. Saya bisa
 * mengetahui predikat apa yang saya dapatkan.
 */
fun predikatTranskrip(nilai: Int) {
    // IF-ELSE IF Expression: Fungsi rangeTo()
    if (nilai in 0.rangeTo(60)) {
        println("Kamu mendapatkan predikat C")
    } else if (nilai in 61.rangeTo(70)) {
        println("Kamu mendapatkan predikat C+")
    } else if (nilai in 71.rangeTo(80)) {
        println("Kamu mendapatkan predikat B")
    } else if (nilai in 81.rangeTo(90)) {
        println("Kamu mendapatkan predikat B+")
    } else if (nilai in 91.rangeTo(100)) {
        println("Kamu mendapatkan predikat A")
    } else {
        println("Kamu mendapatkan predikat D")
    }
}

/**
 * Pada fungsi ini, saya bercerita bahwa saya memiliki seorang Adik yang baru memiliki KTP di bulan
 * Agustus 2021 silam. Ini juga bisa digunakan untuk Adik saya yang masih kecil.
 */
fun adikBikinKTP(umurnya: Int) {
    // IF-ELSE IF Expression: Fungsi until tanpa ()
    if (umurnya in 0 until 17) {
        println("Adik masih dibawah 17 tahun, belum bisa buat KTP")
    } else if (umurnya in 17 until 30) {
        println("Adik sudah 17 tahun, jadi bisa buat KTP")
    } else {
        println("Adik bukan orang Indonesia")
    }
}

/**
 * Pada fungsi ini, saya bercerita tentang Adik saya yang masih berada di jenjang Pendidikan Anak Usia Dini (PAUD).
 * Dengan ini, ia bisa melihat di umur berapa ia akan memasuki jenjang Sekolah Dasar (SD).
 */
fun cekJenjangPendidikan(umurSekarang: Int) {
    // IF-ELSE IF Expression: Fungsi until()
    if (umurSekarang in 0 until (7)) {
        println("Adik sekarang di jenjang Pendidikan Anak Usia Dini (PAUD)")
    } else if (umurSekarang in 7 until (13)) {
        println("Kamu sekarang sudah di jenjang Sekolah Dasar (SD)")
    } else if (umurSekarang in 13 until (16)) {
        println("Kamu sekarang sudah di jenjang Sekolah Menengah Pertama (SMP)")
    } else if (umurSekarang in 16 until (19)) {
        println("Kamu sekarang sudah di jenjang Sekolah Menengah Kejuruan (SMK)")
    } else if (umurSekarang in 19 until (23)) {
        println("Kamu sekarang sudah di jenjang Perguruan Tinggi (PT)")
    } else {
        println("Kamu sudah lulus semua jenjang pendidikan, silahkan cari pekerjaan")
    }
}

// ============================================ WHEN Expression ============================================

/**
 * Pada fungsi ini, saya hanya terinspirasi tentang iklan di bulan puasa.
 */
fun luwakWhiteCoffee(passwordnya: String) {
    // WHEN Expression: Biasa
    when (passwordnya) {
        "Kopi Nikmat Nyaman di Minum" -> println("Halo, Selamat Datang!")
        else -> println("Maaf, password Anda kurang tepat!")
    }
}

/**
 * Pada fungsi ini, saya bercerita bahwa saya sangat suka jajan makanan/minuman. Dari sini, saya bisa tahu
 * saya bisa membeli apa dengan budget sekian.
 */
fun sukaJajan(tapiBudgetmu: Int) {
    // WHEN Expression: Operator Range
    when (tapiBudgetmu) {
        in 10_000..20_000 -> println("Saya bisa beli Thai Tea di Haus!")
        in 20_000..50_000 -> println("Saya bisa beli Boba di XI BO BA")
        in 50_000..100_000 -> println("Saya bisa beli Kopi di Starbucks")
        else -> println("Ga ada yang mau di beli atau emang ga punya uang?")
    }
}

/**
 * Pada fungsi ini, saya bercerita bahwa jika seseorang ingin memasuki Wahana Permainan itu juga harus melihat
 * usia dari pengunjung. Disini bisa mengecek Wahana apa yang cocok sesuai usia pengunjung.
 */
fun syaratWahanaPermainan(cekUsia: Int) {
    // WHEN Expression: Fungsi until tanpa ()
    when (cekUsia) {
        in 10 until 16 -> println("Usia kamu telah mencukupi untuk bermain Turangga Rangga")
        in 16 until 31 -> println("Usia kamu telah mencukupi untuk bermain Roller Coaster atau Bianglala")
        else -> println("Usia kamu belum mencukupi untuk bermain apapun di Wahana ini")
    }
}

/**
 * Pada fungsi ini, saya bercerita tentang seorang Siswa yang ingin mengecek apakah ia telah layak untuk lulus
 * atau tidak. Disini bisa di cek ia harus remedial, dinyatakan lulus, ataupun belum bisa dinyatakan lulus.
 */
fun cekKelulusan(nilaiKeseluruhan: Int) {
    // WHEN Expression: Fungsi until()
    when (nilaiKeseluruhan) {
        in 60 until (70) -> println("Ayo ikuti remedial! Nilai kamu masih bisa diperbaiki")
        in 80 until (100) -> println("Selamat, kamu sudah lulus!")
        else -> println("Maaf, kamu belum bisa dinyatakan lulus")
    }
}